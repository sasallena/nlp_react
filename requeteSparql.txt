PREFIX  dbo:  <http://dbpedia.org/ontology/>
PREFIX  dbpedia-owl: <http://dbpedia.org/ontology/>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  dbpedia: <http://dbpedia.org/resource>
PREFIX  dbpprop: <http://dbpedia.org/property>
PREFIX  foaf: <http://xmlns.com/foaf/0.1/>

SELECT DISTINCT  ?name
WHERE
	{ ?city     rdf:type     dbo:City ;
				rdfs:label   ?citylabel ;
                dbo:country  ?country .
      ?country  rdfs:label   ?countrylabel .
      ?place    rdf:type     dbo:Place .
      ?x        foaf:name    ?name
      FILTER regex(?name, "Tarbes")
      FILTER ( ( lang(?countrylabel) = "fr" ) && ( lang(?citylabel) = "fr" ) )
    }