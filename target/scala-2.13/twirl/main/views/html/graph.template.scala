
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object graph extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(id :Long, name :String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),_display_(/*3.2*/main("Welcome")/*3.17*/ {_display_(Seq[Any](format.raw/*3.19*/("""
"""),_display_(/*4.2*/defining(play.core.PlayVersion.current)/*4.41*/ { version =>_display_(Seq[Any](format.raw/*4.54*/("""

"""),format.raw/*6.1*/("""<section id="content">
    <h1>graph id : """),_display_(/*7.21*/id),format.raw/*7.23*/("""</h1>
    <p>"""),_display_(/*8.9*/graph),format.raw/*8.14*/("""</p>
</section>
	""")))}),format.raw/*10.3*/("""
""")))}))
      }
    }
  }

  def render(id:Long,name:String): play.twirl.api.HtmlFormat.Appendable = apply(id,name)

  def f:((Long,String) => play.twirl.api.HtmlFormat.Appendable) = (id,name) => apply(id,name)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-05-17T17:05:42.755
                  SOURCE: /home/eisti/Documents/nlpreact_test/app/views/graph.scala.html
                  HASH: d5e93b23e275945122f0d8947e54ddd3a444fcef
                  MATRIX: 734->1|852->26|879->28|902->43|941->45|968->47|1015->86|1065->99|1093->101|1162->144|1184->146|1223->160|1248->165|1296->183
                  LINES: 21->1|26->2|27->3|27->3|27->3|28->4|28->4|28->4|30->6|31->7|31->7|32->8|32->8|34->10
                  -- GENERATED --
              */
          