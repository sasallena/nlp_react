
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object text extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[models.Text,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(t :models.Text):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),_display_(/*3.2*/main("le texte")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
"""),_display_(/*4.2*/defining(play.core.PlayVersion.current)/*4.41*/ { version =>_display_(Seq[Any](format.raw/*4.54*/("""

"""),format.raw/*6.1*/("""<section id="content">
    <h1>text id : """),_display_(/*7.20*/t/*7.21*/.id),format.raw/*7.24*/("""</h1>
    <p>text : """),_display_(/*8.16*/t/*8.17*/.text),format.raw/*8.22*/("""</p>
</section>
""")))}),format.raw/*10.2*/("""
""")))}))
      }
    }
  }

  def render(t:models.Text): play.twirl.api.HtmlFormat.Appendable = apply(t)

  def f:((models.Text) => play.twirl.api.HtmlFormat.Appendable) = (t) => apply(t)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-06-09T22:42:37.262
                  SOURCE: /home/eisti/Documents/nlpreact_test/app/views/text.scala.html
                  HASH: 38f2f1badf6025f7042039d27b895bff511cce07
                  MATRIX: 733->1|843->18|870->20|894->36|933->38|960->40|1007->79|1057->92|1085->94|1153->136|1162->137|1185->140|1232->161|1241->162|1266->167|1313->184
                  LINES: 21->1|26->2|27->3|27->3|27->3|28->4|28->4|28->4|30->6|31->7|31->7|31->7|32->8|32->8|32->8|34->10
                  -- GENERATED --
              */
          