// @GENERATOR:play-routes-compiler
// @SOURCE:/home/eisti/Documents/nlpreact_test/conf/routes
// @DATE:Tue Jun 09 23:09:51 CEST 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers {

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:15
  class ReverseGraphController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def graph(n:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "graphs" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("n", n)))))
    }
  
    // @LINE:17
    def changegraph(graph_id:Long, ngraph:String): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "graphs/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("graph_id", graph_id)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("ngraph", ngraph)))))
    }
  
  }

  // @LINE:10
  class ReverseTextController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def changetext(text_id:Long, text:String): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "texts/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("text_id", text_id)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("text", text)))))
    }
  
    // @LINE:10
    def new_text(text:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "texts" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("text", text)))))
    }
  
    // @LINE:11
    def FindById(text_id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "texts/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("text_id", text_id)))
    }
  
  }


}
