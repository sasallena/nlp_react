// @GENERATOR:play-routes-compiler
// @SOURCE:/home/eisti/Documents/nlpreact_test/conf/routes
// @DATE:Tue Jun 09 23:09:51 CEST 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGraphController GraphController = new controllers.ReverseGraphController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseTextController TextController = new controllers.ReverseTextController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGraphController GraphController = new controllers.javascript.ReverseGraphController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseTextController TextController = new controllers.javascript.ReverseTextController(RoutesPrefix.byNamePrefix());
  }

}
