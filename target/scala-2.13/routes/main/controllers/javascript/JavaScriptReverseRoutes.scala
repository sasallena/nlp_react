// @GENERATOR:play-routes-compiler
// @SOURCE:/home/eisti/Documents/nlpreact_test/conf/routes
// @DATE:Tue Jun 09 23:09:51 CEST 2020

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers.javascript {

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:15
  class ReverseGraphController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def graph: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GraphController.graph",
      """
        function(n0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "graphs" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("n", n0)])})
        }
      """
    )
  
    // @LINE:17
    def changegraph: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GraphController.changegraph",
      """
        function(graph_id0,ngraph1) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "graphs/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("graph_id", graph_id0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("ngraph", ngraph1)])})
        }
      """
    )
  
  }

  // @LINE:10
  class ReverseTextController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def changetext: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.TextController.changetext",
      """
        function(text_id0,text1) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "texts/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("text_id", text_id0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("text", text1)])})
        }
      """
    )
  
    // @LINE:10
    def new_text: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.TextController.new_text",
      """
        function(text0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "texts" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("text", text0)])})
        }
      """
    )
  
    // @LINE:11
    def FindById: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.TextController.FindById",
      """
        function(text_id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "texts/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("text_id", text_id0))})
        }
      """
    )
  
  }


}
