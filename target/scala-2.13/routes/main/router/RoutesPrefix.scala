// @GENERATOR:play-routes-compiler
// @SOURCE:/home/eisti/Documents/nlpreact_test/conf/routes
// @DATE:Tue Jun 09 23:09:51 CEST 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
