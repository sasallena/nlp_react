// @GENERATOR:play-routes-compiler
// @SOURCE:/home/eisti/Documents/nlpreact_test/conf/routes
// @DATE:Tue Jun 09 23:09:51 CEST 2020

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HomeController_2: controllers.HomeController,
  // @LINE:10
  TextController_1: controllers.TextController,
  // @LINE:15
  GraphController_0: controllers.GraphController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HomeController_2: controllers.HomeController,
    // @LINE:10
    TextController_1: controllers.TextController,
    // @LINE:15
    GraphController_0: controllers.GraphController
  ) = this(errorHandler, HomeController_2, TextController_1, GraphController_0, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_2, TextController_1, GraphController_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """texts""", """controllers.TextController.new_text(text:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """texts/""" + "$" + """text_id<[^/]+>""", """controllers.TextController.FindById(text_id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """texts/""" + "$" + """text_id<[^/]+>""", """controllers.TextController.changetext(text_id:Long, text:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """graphs""", """controllers.GraphController.graph(n:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """graphs/""" + "$" + """graph_id<[^/]+>""", """controllers.GraphController.changegraph(graph_id:Long, ngraph:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_2.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_TextController_new_text1_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("texts")))
  )
  private[this] lazy val controllers_TextController_new_text1_invoker = createInvoker(
    TextController_1.new_text(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TextController",
      "new_text",
      Seq(classOf[String]),
      "POST",
      this.prefix + """texts""",
      """Uploader/modifier un ou plusieurs textes""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_TextController_FindById2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("texts/"), DynamicPart("text_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_TextController_FindById2_invoker = createInvoker(
    TextController_1.FindById(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TextController",
      "FindById",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """texts/""" + "$" + """text_id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_TextController_changetext3_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("texts/"), DynamicPart("text_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_TextController_changetext3_invoker = createInvoker(
    TextController_1.changetext(fakeValue[Long], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TextController",
      "changetext",
      Seq(classOf[Long], classOf[String]),
      "PUT",
      this.prefix + """texts/""" + "$" + """text_id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_GraphController_graph4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("graphs")))
  )
  private[this] lazy val controllers_GraphController_graph4_invoker = createInvoker(
    GraphController_0.graph(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GraphController",
      "graph",
      Seq(classOf[String]),
      "POST",
      this.prefix + """graphs""",
      """Uploader/modifier un ou plusieurs graphes""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_GraphController_changegraph5_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("graphs/"), DynamicPart("graph_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GraphController_changegraph5_invoker = createInvoker(
    GraphController_0.changegraph(fakeValue[Long], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GraphController",
      "changegraph",
      Seq(classOf[Long], classOf[String]),
      "PUT",
      this.prefix + """graphs/""" + "$" + """graph_id<[^/]+>""",
      """GET 	/graphs/:graph_id					controllers.TextController.findById(graph_id : Long)""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_2.index)
      }
  
    // @LINE:10
    case controllers_TextController_new_text1_route(params@_) =>
      call(params.fromQuery[String]("text", None)) { (text) =>
        controllers_TextController_new_text1_invoker.call(TextController_1.new_text(text))
      }
  
    // @LINE:11
    case controllers_TextController_FindById2_route(params@_) =>
      call(params.fromPath[Long]("text_id", None)) { (text_id) =>
        controllers_TextController_FindById2_invoker.call(TextController_1.FindById(text_id))
      }
  
    // @LINE:12
    case controllers_TextController_changetext3_route(params@_) =>
      call(params.fromPath[Long]("text_id", None), params.fromQuery[String]("text", None)) { (text_id, text) =>
        controllers_TextController_changetext3_invoker.call(TextController_1.changetext(text_id, text))
      }
  
    // @LINE:15
    case controllers_GraphController_graph4_route(params@_) =>
      call(params.fromQuery[String]("n", None)) { (n) =>
        controllers_GraphController_graph4_invoker.call(GraphController_0.graph(n))
      }
  
    // @LINE:17
    case controllers_GraphController_changegraph5_route(params@_) =>
      call(params.fromPath[Long]("graph_id", None), params.fromQuery[String]("ngraph", None)) { (graph_id, ngraph) =>
        controllers_GraphController_changegraph5_invoker.call(GraphController_0.changegraph(graph_id, ngraph))
      }
  }
}
