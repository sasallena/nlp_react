package events

import java.time.Instant



sealed trait GraphEvent{
	def date(): Instant
}


case class GraphCreated(id:Long,Graph:String,date:Instant=Instant.now()) extends GraphEvent{}

case class GraphChanged(id:Long, newGraph:String, date:Instant=Instant.now()) extends GraphEvent{} 

case class GraphDeleted(id:Long, date:Instant=Instant.now()) extends GraphEvent{}