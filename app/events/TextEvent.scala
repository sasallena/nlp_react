package events

import java.time.Instant


sealed trait TextEvent{
	def date(): Instant
}


case class TextCreated(id:Long,text:String,date:Instant=Instant.now()) extends TextEvent{}

case class TextChanged(id:Long, newtext:String, date:Instant=Instant.now()) extends TextEvent{} 

case class TextDeleted(id:Long, date:Instant=Instant.now()) extends TextEvent{}