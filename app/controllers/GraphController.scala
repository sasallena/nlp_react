package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json.{JsError, JsPath, JsSuccess, JsValue, Json}
import services.GraphService
import models.Graph

@Singleton
class GraphController@Inject()(graphService: GraphService,cc:ControllerComponents) extends AbstractController(cc){
	def graph(n:String) = Action{
		implicit request: Request[AnyContent] => {
			Ok((graphService.addNewGraph(n)).toString)
		}
	}
	def changegraph(id:Long, ng:String)=Action{
		implicit request: Request[AnyContent] => {
			    graphService.changeGraph(id, ng) match {
      			case None => NotFound
      			case Some(u) => Ok(Json.toJson(u))
    }
		}
	}
	def delete(id:Long)=Action{
		implicit request : Request[AnyContent]=>{
			graphService.delete(id)
			NoContent
		}
	}

}
