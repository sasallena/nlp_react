package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json.{JsError, JsPath, JsSuccess, JsValue, Json}

import services.TextService
import models.Text

@Singleton
class TextController@Inject()(textService: TextService,cc:ControllerComponents) extends AbstractController(cc){
	def new_text(n:String) = Action{
		implicit request => {
			Ok(textService.addNewText(n).toString)
		}
	}

	def FindById (id: Long) = Action {
    	val text = textService.findById(id)
    	text match {
      		case None => Ok(views.html.error404())
      		case Some(u) => Ok(views.html.text(u))
    	}
	}

	def changetext(id:Long, nn:String)=Action{
		implicit request: Request[AnyContent] => {
			    textService.changeText(id, nn) match {
      			case None => NotFound
      			case Some(u) => Ok(Json.toJson(u))
    }
		}
	}
	def delete(id:Long)=Action{
		implicit request =>{
			textService.delete(id)
			NoContent
		}
	}
}