package services

import javax.inject.{Inject, Singleton}
import events.{GraphEvent, GraphCreated,GraphChanged,GraphDeleted}
import scala.concurrent.ExecutionContext
import models.Graph


@Singleton
case class GraphService@Inject() () {
	var graphEventLog:Seq[GraphEvent] = Seq.empty[GraphEvent]
	var _id : Long= -1
	def nextId = {
		_id = _id + 1
		_id
	}

	def addNewGraph(graph:String) = {
		val __id: Long = nextId
		graphEventLog = graphEventLog:+GraphCreated(__id,graph)
		__id
	}

	def delete(id:Long):Boolean = {
		val user: Option[Graph] = findById(id)
		user match{
			case Some(_) => graphEventLog = graphEventLog :+GraphDeleted(id)
			true
			case None => false 
		}

	}

	def changeGraph(id:Long, newgraph:String): Option[Graph] = {
		val graph: Option[Graph] = findById(id)
		graph match{
			case Some(g) => graphEventLog = graphEventLog :+ GraphChanged(id, newgraph)
			Some(g.copy(graph=newgraph))
			case None => None
		}
	}

	def findById(id:Long): Option[Graph] = {
		val lu: Seq[Graph] = listGraph()
		lu.find(graph => graph.id==id)
	}

	def listGraph(): Seq[Graph] = {
		def parseEvent(s: Seq[Graph], e: GraphEvent) : Seq[Graph]={
			e match {
				case GraphCreated(id,t,_) => s:+Graph(id, t)
				case GraphDeleted(id, _) => s.filter({a => a.id != id})
				case GraphChanged(id, nn, _) => s.map({a => if (a.id == id) a.copy(graph=nn) else a})
				case _ => s
			}
		}
		graphEventLog.foldLeft(Seq.empty[Graph])(parseEvent)
	}
}