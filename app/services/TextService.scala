package services

import javax.inject.Singleton
import events.{TextEvent, TextCreated, TextDeleted, TextChanged}
import models.Text

@Singleton
case class TextService(){
	var textEventLog:Seq[TextEvent] = Seq.empty[TextEvent]
	var _id : Long= -1
	def nextId = {
		_id = _id + 1
		_id
	}

	def addNewText(text:String) = {
		val __id: Long = nextId
		textEventLog = textEventLog :+TextCreated(__id,text)
		__id
	}

	def delete(id:Long):Boolean = {
		val user: Option[Text] = findById(id)
		user match{
			case Some(_) => textEventLog = textEventLog :+TextDeleted(id)
			true
			case None => false 
		}

	}

	def changeText(id:Long, newtext:String): Option[Text] = {
		val text: Option[Text] = findById(id)
		text match{
			case Some(t) => textEventLog = textEventLog :+ TextChanged(id, newtext)
			Some(t.copy(text=newtext))
			case None => None
		}
	}

	def findById(id:Long): Option[Text] = {
		val lu: Seq[Text] = listText()
		lu.find(text => text.id==id)
	}

	def listText(): Seq[Text] = {
		def parseEvent(s: Seq[Text], e: TextEvent) : Seq[Text]={
			e match {
				case TextCreated(id,t,_) => s:+Text(id, t)
				case TextDeleted(id, _) => s.filter({a => a.id != id})
				case TextChanged(id, nn, _) => s.map({a => if (a.id == id) a.copy(text=nn) else a})
				case _ => s
			}
		}
		textEventLog.foldLeft(Seq.empty[Text])(parseEvent)
	}
}