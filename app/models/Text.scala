package models

import play.api.libs.json._

case class Text(id:Long,text:String)
{

}

case class TextInfo(text:String)
{
	
}

object Text{
	implicit val format:OFormat[Text] = Json.format[Text]
}

object TextInfo{
	implicit val format:OFormat[TextInfo] = Json.format[TextInfo]
}