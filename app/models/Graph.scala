package models

import play.api.libs.json._

case class Graph(id:Long,graph:String)
{

}

case class GraphInfo(graph:String)
{
	
}

object Graph{
	implicit val writes = Json.writes[Graph]
	val format:OFormat[Graph] = Json.format[Graph]
}

object GraphInfo{
	val format:OFormat[GraphInfo] = Json.format[GraphInfo]
}